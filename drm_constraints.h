#ifndef DRM_CONSTRAINTS_H
#define DRM_CONSTRAINTS_H

#include <libdrm/drm.h>

#define DRM_CONSTRAINT_ADDR_ALIGN 1
#define DRM_CONSTRAINT_STRIDE_MAX 2

/**
 * drm_constraint_read - Read a single constraint from a constraint set.
 * @type: filled with the constraint type
 * @size: filled with the constraint body size, in bytes
 * @data_ptr: filled with a pointer to the constraint body
 * @set: the capability set
 * @set_size: the capability set size, in bytes
 *
 * Returns: the number of bytes read from the constraint set
 */
static __u64
drm_constraint_read(__u32 *type, __u32 *size, const void **data_ptr,
		    const void *set, __u64 set_size)
{
	const __u64 *set_u64 = set;
	__u64 constraint_size;

	if (set_size < 8) {
		return 0;
	}
	*type = (__u32)(set_u64[0] & 0xFFFFFFFF);
	*size = (__u32)(set_u64[0] >> 32);

	constraint_size = 8 + *size;
	if (constraint_size > set_size || *size % 8 != 0) {
		return 0;
	}

	*data_ptr = &set_u64[1];
	return constraint_size;
}

/**
 * drm_constraint_write - Write a constraint to a constraint set.
 * @out: pointer to the next empty slot in the constraint set
 * @out_size: the number of bytes remaining in the constraint set
 * @type: the type of the constraint
 * @size: the size of the constraint body, in bytes
 * @data: the constraint body
 *
 * If out is NULL, this function just returns the size of the constraint
 * without writing it.
 *
 * Returns: the number of bytes written to the constraint set
 */
static __u64
drm_constraint_write(void *out, __u64 out_size, __u32 type, __u32 size,
		     const void *data)
{
	const __u64 *data_u64 = data;
	__u64 *out_u64 = out;
	__u64 constraint_size = 8 + size;
	__u32 i;

	if (!out) {
		return constraint_size;
	}
	if (out_size < constraint_size || size % 8 != 0) {
		return 0;
	}

	out_u64[0] = (__u64)type | ((__u64)size << 32);
	for (i = 0; i < size / 8; ++i) {
		out_u64[1 + i] = data_u64[i];
	}

	return constraint_size;
}

static __u64
_drm_constraint_gcd(__u64 a, __u64 b)
{
	__u64 tmp;

	while (b > 0) {
		tmp = b;
		b = a % b;
		a = tmp;
	}

	return a;
}

static __u64
_drm_constraint_lcm(__u64 a, __u64 b)
{
	return (a * b) / _drm_constraint_gcd(a, b);
}

/**
 * drm_constraint_merge - Merge two constraint sets
 * @out: buffer where the output constraint set will be written to
 * @out_size: size of the output buffer, in bytes
 * @a: first constraint set
 * @a_size: size of the first constraint set, in bytes
 * @b: second constraint set
 * @b_size: size of the second constraint set, in bytes
 *
 * If out is NULL, this function returns the size of the output constraint set
 * without writing it.
 *
 * Returns: the size of the output constraint set, in bytes
 */
static __u64
drm_constraint_merge(void *out, __u64 out_size, const void *a, __u64 a_size,
		     const void *b, __u64 b_size)
{
	__u32 type, size;
	__u64 merged_size = 0, nread, nwritten, i, cur_size;
	const void *cur, *data;
	const __u64 *data_u64;

	__u64 addr_align = 0;
	__u64 stride_max = 0;

	/* Process both of the input constraint sets, one at a time */
	for (i = 0; i < 2; ++i) {
		if (i == 0) {
			cur = a;
			cur_size = a_size;
		} else {
			cur = b;
			cur_size = b_size;
		}

		/* Apply constraints for the current constraint set */
		while ((nread = drm_constraint_read(&type, &size, &data,
						    cur, cur_size))) {
			data_u64 = data;
			switch (type) {
			case DRM_CONSTRAINT_ADDR_ALIGN:
				if (size != 8) {
					return 0;
				}
				if (addr_align == 0) {
					addr_align = data_u64[0];
				} else {
					addr_align =
						_drm_constraint_lcm(addr_align,
								    data_u64[0]);
				}
				break;
			case DRM_CONSTRAINT_STRIDE_MAX:
				if (size != 8) {
					return 0;
				}
				if (data_u64[0] < stride_max ||
				    stride_max == 0) {
					stride_max = data_u64[0];
				}
				break;
			default:
				return 0;
			}
			cur_size -= nread;
			cur = (const __u8 *)cur + nread;
		}
	}

	/* Now that we've collected all constraints, write them back in a new
	 * constraint set */

	if (addr_align > 0) {
		nwritten = drm_constraint_write(out, out_size,
						DRM_CONSTRAINT_ADDR_ALIGN,
						sizeof(addr_align),
						&addr_align);
		merged_size += nwritten;
		if (out) {
			out_size -= nwritten;
			out = (__u8 *)out + nwritten;
		}
	}
	if (stride_max > 0) {
		nwritten = drm_constraint_write(out, out_size,
						DRM_CONSTRAINT_STRIDE_MAX,
						sizeof(stride_max),
						&stride_max);
		merged_size += nwritten;
		if (out) {
			out_size -= nwritten;
			out = (__u8 *)out + nwritten;
		}
	}

	return merged_size;
}

#endif
