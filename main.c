#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "drm_constraints.h"

static size_t
write_constraint_set_a(void *out, size_t out_size)
{
	uint64_t addr_align = 32;

	return drm_constraint_write(out, out_size, DRM_CONSTRAINT_ADDR_ALIGN,
					 sizeof(addr_align), &addr_align);
}

static size_t
write_constraint_set_b(void *out, size_t out_size)
{
	uint64_t addr_align = 64;
	uint64_t stride_max = 2048;

	size_t nwritten, size = 0;

	nwritten = drm_constraint_write(out, out_size, DRM_CONSTRAINT_ADDR_ALIGN,
					sizeof(addr_align), &addr_align);
	size += nwritten;
	if (out) {
		out = (uint8_t *)out + nwritten;
		out_size -= nwritten;
	}
	nwritten = drm_constraint_write(out, out_size, DRM_CONSTRAINT_STRIDE_MAX,
					sizeof(stride_max), &stride_max);
	size += nwritten;
	if (out) {
		out = (uint8_t *)out + nwritten;
		out_size -= nwritten;
	}
	return size;
}

static void
print_constraints(const void *set, size_t set_size)
{
	uint32_t type, size;
	const void *data;
	const uint64_t *data_u64;
	size_t nread;

	while ((nread = drm_constraint_read(&type, &size, &data,
					    set, set_size))) {
		data_u64 = data;
		switch (type) {
		case DRM_CONSTRAINT_ADDR_ALIGN:
			printf("ADDR_ALIGN(%" PRIu64 ") ", data_u64[0]);
			break;
		case DRM_CONSTRAINT_STRIDE_MAX:
			printf("STRIDE_MAX(%" PRIu64 ") ", data_u64[0]);
			break;
		default:
			printf("<unknown> ");
		}
		set_size -= nread;
		set = (const __u8 *)set + nread;
	}
	printf("\n");
}

int
main(int argc, char *argv[])
{
	size_t set_a_size, set_b_size, set_ab_size;
	void *set_a, *set_b, *set_ab;

	set_a_size = write_constraint_set_a(NULL, 0);
	set_a = malloc(set_a_size);
	write_constraint_set_a(set_a, set_a_size);

	set_b_size = write_constraint_set_b(NULL, 0);
	set_b = malloc(set_b_size);
	write_constraint_set_b(set_b, set_b_size);

	printf("Constraint set A: ");
	print_constraints(set_a, set_a_size);
	printf("Constraint set B: ");
	print_constraints(set_b, set_b_size);

	set_ab_size = set_a_size + set_b_size;
	set_ab = malloc(set_ab_size);
	set_ab_size = drm_constraint_merge(set_ab, set_ab_size, set_a,
					   set_a_size, set_b, set_b_size);
	if (set_ab_size == 0) {
		fprintf(stderr, "Failed to merge sets\n");
		return 1;
	}

	printf("Merged constraint set: ");
	print_constraints(set_ab, set_ab_size);

	free(set_ab);
	free(set_b);
	free(set_a);
	return 0;
}
